--Ad: Melumat artiqligini yaratmadan melumatlarin melumat bazasindaki cedvellere elave edilmesi ucun , bu cedvellerin yaradilmasina aid qaydalar toplusu
--Qaydalar siyahisi:
--1.Cedvellerdeki sutunlar qruplasdirilmali [vizual mushahide]
--2.Entity-ler arasi vacibklik derecesini mueyyen etmek [Ne neden asillidir ]
--3.Entity-ler arasi elaqelerin qurulmasi [ER 1-1, 1-N,N-M]



--Numune:  melumat bazasina mektebe aid melumatlarin elave edilmesi ucun, mekteb melumatlari toplusu.
--Mekteb adi	Mekteb unvan	Sinif	Muellim adi	Muellim Soyad	Muellim Tevellud	Shagird adi	Shagird Soyad	Shagird Tevellud	Shagird Qiymet	Tarix

--Bu cedvel qaydalara esasen qurulmadigindan, bu cedvele melumatlari elave etdikde melumat artiqligi yaranir.
--Bu problemi aradan qaldirmaq ucun vahid cedveli bir nece cedvellere ayirmaq lazimdir.
--Bu bolgunu aparmaq ucun bir nece qaydani tetbiq edeceyik
--1.Cedveldeki sutunlari qruplasdirmaga calismaliyiq
--Burada bir nece sutunlarin bir qrup adi altinda ifade etmek olar. misal olaraq, Muellime aid sutunlari Muellim qrupuna , Shagirde aid melumatlari shagird qrupuna elave etmek olar
--Bu qruplasma zamani diqqet olunacaq bir meqam var. Shagirde aid qiymet ve tarix sutunlarini da shagird qrupuna elave edilmelidir yoxsa ayrica qrupa elave edilmelidir?
--Eger her hansi qrup uzre hemin qrup melumatlara gore hereketlilik varsa, baxmayaraq ki, sutun adlari, melumatlarin eyni qrupdan olmasini gosterir, biz hemin hereketliyiye aid
--olan melumatlari ferqli qrupa elave etmeliyik.
--Yani bizim misalimizda Shagird uzre 2 qrup yaranacaq Shagird  ve X qrupu. Bu X qrupuna biz Shagird qiymetlendirmesi ve ya Qiymetlendirme cedveli ve s kimi de ad vere bilerik.
--X qrupuna mutleq sekilde Shagirdle bagli adin verilmesi mecbur deyil. 
--Teklif edirem,X qrupuna Qiymetmenlendirme  adi verek
--Qruplasma neticesinde Mekteb, Sinif,Muellim,Shagird ,Qiymetlendirme  entity- lerimiz olacaq.
--2.Bu qruplasma zamani yaranan Entity lerin vaciblik derecesini mueyyen etmek
--Ilk novbede calismaliyiq bu entitylerde neyin neden asili oldugunu mueyyen ederek, entity ler arasinda vaciblik derecesini mueyyen etmek.
--Qeyd etdiytimiz entity-ler arasinda daha vacib olan mekteb-dir,ona gore ki, digerler entity lerin varligi mektebden asilidir. 
--Bundan sonra vacib olani sinif-dir. Mekteb daxilinde sinif olmasa, dersler olmayacaq. Bu derslerde ders deyen muellim ve shagirdeler olmayacaq.
--Diger vacib olan, muellim-dir. Muellim olmasa telimler olmayacaq. ve telim olmayan mektebde shagird olmayacaq
--Son olaraq, vacib olan Shagirdi.Shagird olmasa  qiymetlendirme olmayacaq
--Bu vacibklik sirasina gore entityleri siralasaq, Mekteb, Sinif, Muellim, Shagird, Qiymetlendirme  olacaqdir.
--3.Entitylerin arasinda elaqelerin yaradilmasi
--Vaciblik derecesini esas tutaraq, elaqeleri yaradaq
--Mekteb ve sinif ettitylerin elaqelerine baxsaq, gorerik ki, bir mektebde bir nece sinifin olmasi mumkun oldugu ucun bu ER 1-N elaqelendirilmesine aid olacaq.
--Yani 1 mektebe N sayda sinif baglidir.
--Sinif ve Muellim entitylerine baxsaq, burada gorerik ki, bir sinife bir nece muellim bagli ola, elece de bir muellim bir nece sinife bagliligi mumkun olduqu ucun ER N-M 
--elaqelendirilmesi mumkun olacaq
--Sinif ve Shagird entitylerine baxsaq, burada gorerik ki,bir bir sinif  bir nece shagirde bagli olmasi mumkun oldugu ucun bu ER 1-N elaqelendirilmesi olacaq
--Shagird ve Qiymetlendirme entitylerine baxsaq,burada gorerik ki, bir shagird bir nece qiymetlendirmeye bagli olmasi mumkun olduqu ucun bu ER 1-N elaqelendirilmesi olacaq.
--Yuxarida yazilan elaqelendirmeleri siyahilasdiraq
--Mekteb ve sinif 1-N
--Sinif ve Muellim N-M 
--Sinif ve Shagird 1-N
--Shagird ve Qiymetlendirme 1-N

--Burada biz 1 sayda N-M Elaqelendirmesini goruruk. Bu o demek dir ki, bu elaqlendirme novu oldugu ucun entityler uzre cedvelleri yaratdigimiz zamani,bir cedveli artiq yaratmali olacagiq

--4. Entityler uzre yaranacaq cedvelleri ve onlarin sutunlarini mueyyen edek
--Mekteb
    --1.School          -Name, Address
    --2.Class           -Name,
    --3.Teacher         -Name,Surname, Birthdate
    --4.Student         -Name,Surname, Birthdate
    --5.Mark            -Mark,Mark_Date
    --6.ER N-M gore yaranacaq elave cedvel haqda ashagida melumat verilecek.

--5.Yeni cedvellerde elaqelendirme ile bagli acar sutunlari da elave etmek
    --1.School cedveline istinad(Class cedvelinden) olundugu ucun bu cedvelde ID adli sutun elave olunmalidir.
    --2. Class ve Teacher arasinda N-M elaqesi oldugu ucun CLASS_TEACHER adli cedvel yaradilmali ve bu cedvel hem (Class cedveline) istinad etdiyi ucun CLASS_ID
         -- hem de (Teacher cedveline) istinad etdiyi ucun TEACHER_ID sutunlari elave olunmalidir.
    --3.Class cedveline istinad(CLASS_TEACHER cedvelinden) olundugu ucun bu cedvele ID adli sutun, elece de bu cedvel (School cedveline) istinad etdiyi ucun SCHOOL_ID sutunu elave olunmalidir.
    --4.Teacher cedveline istinad(CLASS_TEACHER cedvelinden) olundugu ucun bu cedvele ID adli sutun,elave edilmelidir.
    --5.Student cedveline istinad(Mark cedvelinden) olundugu ucun bu cedvele ID adli sutun elece de bu cedvel (Class cedveline) istinad etdiyi  ucun  CLASS_ID sutunu elave olunmalidir.
    --6.Mark cedveli istinad(Student cedveline) etdiyi ucun STUDENT_ID sutunu elave olunmalidir.
     
--6. Elave sutunlari nezere alaraq, yeniden cedveller uzre sutunlari mueyyen edek. Bu zaman , ID-lerle bagli sutunlari ilk siralarda teyin edek. Esas ID ilk, daha sonra toreme ID ler olsun

    --1.School
      /*
           CREATE TABLE SCHOOL( 
                                ID          NUMBER,
                                NAME        VARCHAR2(30),
                                ADDRESS     VARCHAR2(100)
                              );
      */
     
     --2.CLASS_TEACHER
       /*
       
          CREATE TABLE CLASS_TEACHER(
                                CLASS_ID    NUMBER,
                                TEACHER_ID  NUMBER,
                               
                            );    
       
       */
        
     
     --2.Class
       /*
       
          CREATE TABLE CLASS(
                                ID          NUMBER,
                                SCHOOL_ID   NUMBER,
                                NAME        VARCHAR2(50)
                            );    
       
       */
       
     --3.Teacher 
        /*
          
          CREATE TABLE TEACHER(
                                ID    NUMBER,
                                NAME        VARCHAR2(30),
                                SURNAME     VARCHARR2(30),
                                BIRTHDATE   DATE
                              );
        
        */
        
     --4.Student
       /*
         CREATE TABLE STUDENT(
         
                              ID            NUMBER,
                              CLASS_ID      NUMBER,
                              NAME          VARCHAR2(30),
                              SURNAME       VARCHAR2(30),
                              BIRTHDATE     DATE
                            );
       
       */
     --5.Mark
        /*
         
         CREATE TABLE MARK (
         
                             STUDENT_ID     NUMBER,
                             MARK           NUMBER,
                             MARK_DATE      DATE
                            );
        
        */

--7. Cedvelleri yaradaq
--Her bir cedveli yaratmamisdan once onlari silmek ucun emr verek ki, eger eyni adli cedvel movcud olarsa, yenisini yaratmaq mumkun olsun deye,
--bunu etmesek, yeni yartmaq istediyimiz cedveli yartmayacaq ve xeta verecek,  eyni adli cedvelin movcud olmasi sebebile.
    --1.School cedveli 
     --
     drop table school;
     
     --
        CREATE TABLE SCHOOL( 
                                ID          NUMBER,
                                NAME        VARCHAR2(30),
                                ADDRESS     VARCHAR2(100)
                              );
     
   --2.Class cedveli
    --
    drop table class;
    --
     CREATE TABLE CLASS(
                                ID          NUMBER,
                                SCHOOL_ID   NUMBER,
                                NAME        VARCHAR2(50)
                            );    
 
   --3.Teacher cedveli
    --
    drop table teacher;
    --
     CREATE TABLE TEACHER(
                                ID          NUMBER,
                                NAME        VARCHAR2(30),
                                SURNAME     VARCHAR2(30),
                                BIRTHDATE   DATE
                              );
    --4.Class_Teacher cedveli Elaqelendirme cedvellerini her zaman elaqeli cedveller yaradildiqdan sonra, yaratmaq lazimdir. 
    --Bunun vacib oldugu meqami cedvelleri constratinlerle birge yaratdigimiz zamani gormus olacagiq. 
    --
    drop table class_teacher;
    --
     CREATE TABLE CLASS_TEACHER(
                                CLASS_ID          NUMBER,
                                TEACHER_ID        NUMBER
                              
                              );
   
   
   
   --4.Student cedveli                          
   --
   drop table student;
   --
    CREATE TABLE STUDENT(
         
                              ID            NUMBER,
                              CLASS_ID      NUMBER,
                              NAME          VARCHAR2(30),
                              SURNAME       VARCHAR2(30),
                              BIRTHDATE     DATE
                            );
  --5.Mark cedveli
   --
   drop table mark;
   --
    CREATE TABLE MARK (
         
                             STUDENT_ID     NUMBER,
                             MARK           NUMBER,
                             MARK_DATE      DATE
                            );
                            
                            
                            
--8. Cedvellere melumatlari elave edek.
   --Diqqet. Eger cedvellere elave etdiyimiz melumatlarin  cedvellerde saxlanmasi ucun her bir DML emeliyatindan sonra COMMIT  emri verilmelidir.
            --Eks halda  SQL DEVELOPER-e yeniden daxil olsaq, biz o melumatlari gormeyeceyik. Bu melumatlarin COMMIT olmadan gorunmesi ,lakin yeniden daxil olanda gorunmemesi sebebi,
            --bu melumatlarin sessiya seviyyesinde saxlanilmasi, ORACLE terefinden diske yazilmamasindandir. Eger biz melumatlarin uzunmuddetli diskde qalmasini isteyirikse,
            --o zaman her DML emrinden sonra COMMIT emrini de icra etmeliyik.
            --DDL emrlerinden sonra COMMIT ona gore yazmiriq ki, DDL emeliyyatlarindan sonra ORACLE ozu gizli olaraq, COMMIT emrini icra edir, ve bu sebebden
            --bizim aciq sekilde COMMIT emri yazmagimiza ehtiyyac qalmir.
   --1. School cedveli
        insert into school(id,name,address) values(1,'1 sayli orta mekteb','Ehmedzade 5');
        commit;
        --neticeye baxaq
        select * from school;
   --2. Class cedveli
        insert into class(id,school_id,name) values(1,1,'5A');
        commit;
        --neticeye baxaq
        select * from class;
   --3. Teacher cedveli
        insert into teacher(id,name,surname,birthdate) values(1,'Firuz','Aliyev',to_date('01/01/1980','MM/DD/YYYY'));
        commit;
        --neticeye baxaq
        select * from teacher;
   --4. Class_Teacher cedveli
        insert into class_teacher(class_id,teacher_id) values(1,1);
        commit;
        --neticeye baxaq
        select * from class_teacher;
   --4. Student cedveli
        insert into student(id,class_id,name,surname,birthdate) values(1,1,'Aynur','Mammadova',to_date('01/01/2012','MM/DD/YYYY'));
        commit;
        --neticeye baxaq
        select * from student;
   --5. Mark cedveli
        insert into mark(student_id, mark,mark_date) values(1,5,to_date('01/01/2023','MM/DD/YYYY'));
        commit;
        --neticeye baxaq
        select * from mark;
        
--9. Cedvelleri JOIN edek ve butun bu cedvelleri birlesdirerek, 1-ci setr qeyd olunan   cedvel sekline salaq. JOIN movzusunu daha etrafli novbeti derslede oyreneceyik
     SELECT school.*,class.*,teacher.*,student.*,mark.* 
          FROM school  
             join class on school.id=class.school_id 
             join class_teacher on class_teacher.class_id=class.id
             join teacher on teacher.id=class_teacher.teacher_id
             join student on student.class_id=class.id
             join mark on  mark.student_id=student.id;
     
     --neticede biz ayri-ayri cedvellerde olan melumatlari mehz duzgun qurulmus struktur esasinda JOIN ler vasitesile  bir yerde birlesdire bildik.         
