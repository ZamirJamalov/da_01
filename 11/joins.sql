--tasks 
--add narural and using 


--Join novleri

--Inner vr Outer joins tiplerine bolunurler

--Inner joins connect rows in two or more tables if and only if there
--are matched rows in all the tables being joined

--Outer joins connect rows in two or more tables in a way that is more
--inclusive—if data exists in one table that has no matching values in
--another, the unmatched row will still be included in the output.


/*

Equijoins connect data in two or more tables by looking for common
data among the tables’ columns. In other words, an equijoin looks for an
exact match of data.

Non-equijoins connect data by looking for relationships that don’t
involve equality, such as “less than” or “greater than” relationships, or
situations where data in one table is within a range of values in another.
*/


JOINS

  INNER JOIN/EQUI JOIN/JOIN 2  cedvel uzre uygun melumatlar esasinda
  her iki cedvelden neticeni qaytarir.
  misal olaraq 
  
*/
select * from employees join departments 
   on employees.department_id=departments.department_id;
   
 --neticeye diqqet etsek, gorerik ki, her iki cedvele aid sutunlar uzre
 --melumatlar eks olunur. Bur de diqqet etsek her iki cedvelde 
 --olan eyni adli sutunlar uzre sutun adlarinda ferqliliyi temin etmek ucun
 --elave nomrelenme olunubdur. meselen department_id ve manager_id sutunlari
 --departments cedvelinin sutunlarinda nomrelenib.
 
 --Bu melumatlarin  eks olunmasi her iki sutunda eyni adli sutun adlari 
 --deyil, o sutunlarin her setrinde yerlesen deyerlerin eyni olmasi ile elaqedar
 --biz bu birlesmeni elde etmisik. Ola biler ki, departments  cedvelindeki 
 --department_id sutunun adi basqa olsun. ancaq hemin sutundaki deyerler
 --eyni olaraq qalarsa o zaman biz yene de bu melumatlari ekranda goreceyik.
 --baxmyaraq ki , sutun adlari ferqlidir.
 --misal olaraq, ferqli sutunlar uzre melumatli birlesdire ve yeni 
 --melumatlari ekranda gore  bilerik. ancaq baxmayaraq ki, ekranda netice gorunur
 --birlesme mentiqi olmadigi ucun neticede mentiqe uygun deyil.
 --misal olaraq
 select * from employees join departments 
    on employees.salary=departments.location_id;
 --tesadufen emek haqqisi departamentin location id ne beraber olan
 --bu melunmat coxlugu eslinden bir mena kesb etmir. bele ki yanlis elaqelendirilme 
 --neticesinde melumat coxlugu elde olunubdur.
 --misal olaraq bu coxduqdan olan 119 id ki emekdasin departament id si burada 
 --gorsendiyi kimi 80 deyil ,  30 dur. Ve o Sales departmaneti deyil,Purchasing
 --adli departamentinde calisir.
 --Bununla onu demak olarki, join vasitesile eger her hansi netice elde olunarsa
 --bu hele  gorulen isin dogrulugunu subut etmir. Her bir halda
 --biz neticenin dogrulunu bu ve ya diger sekilde yoxlamagi bacarmali ve diqqetde
 --saxlamaliyiq.
 
  select department_id from employees where employee_id=119;
 select * from departments where department_id=30;
 
 /*
  join neticesi olaraq ekranda gorunen sutun adlarini nomrelenmis ve ya 
  umumiyyetle ferqli adda  gostermek isteyirikse o zaman 
  alias lardan istifade etmeliyik.
  sutun adlarinda alias 2 novde istifade oluna biler.
  eger alias  bir ve ya bir birine bitisik bir nece sozden ibaret olarsa
  o zaman alias oldugu sekilde yazilir. mes. departtament_adi
  eger alias bir nece sozden ibaret ve bu sozler arasinda bosluq olmasini 
  isteyirik se o zaman " cut dirnaqdan istifade  olunmali.
  mes. "departament adi"
  
  alias tek  join vasitesile cedcellleri birlesdirdiyimiz zaman deyil 
  hem de tek cedvelden sorgu cekdiyimiz zamanda istifade oluna biler.
 */
 select department_name,department_name as "Departament adı" from departments;
 
 select department_name, department_name as departament_adı from departments;
 
 --bu xeta verecek
 select department_name, department_name as departament adı from departments;
 --bu da xeta verecek
 select department_name, department_name as 'departament adı' from departments;
 
 --AS acar sozunu  yaznmaq mecburi deyil. bu xeta vermeyecek.
 select department_name , department_name  "departament adı" from departments;
 
 --sutun adina alias verdikde where ve ta join bolmesinden alias adi deyil
 --hemin sutunun adi qeyd olunmalidir. inner sql kecdikde aliasa muracietin 
 --olunmasinin zeruriliyini goreceyik. ancaq indiki halda bizde bu 
 --zeruriyyet teleb olunmur.
 --misal olaraq bu xeta verecek 
 select first_name  as "Əməkdaş adı" from employees where "Əməkdaş adı"='Steven';
 --bu xeta vermeyecek
 select first_name as "Əməkdaş adı" from employees where first_name='Steven';
 
 /*
  alias -lar sutun adlarina verildiyi kimi cedvellere de verilir.
  misal olaraq
 */
 select first_name from employees  e;
 
--cedvel adina alias verdikde  sutundaki kimi AS acar sozu yazilmir.
--mes bu xeta verecek.
select first_name from employees as e;

--cedvel adlarina verilen alias adeten bir nece simvoldan ibaret olub 
--her zaman uzun uzadi cedvelin adini tekralamirlar
--mes
select employees.first_name,employees.last_name,departments.department_name
 from employees  join departments on employees.department_id=departments.department_id;
 
--qisa olaraq bele de yaza bilerik
select e.first_name,e.last_name,d.department_name from employees e 
 join departments d on  e.department_id=d.department_id; 
 
 --eger cedvellere alias verildiyi halda sutun adlari qarsisinda eyni
 --zamanda cedvel adindan istifade  olunarsa bu zaman xeta verecek 
 --misal olaraq
 select e.first_name,employees.last_name, d.department_name
  from employees e join departments d on e.department_id=d.department_id;

--Eger alias tetbiq olunubsa o zaman ancaq alias dan istifade etmek lazimdir
--hem cedvel adi hem de aliasdan istifadeye icaze verilmir.


--Umumiyyetle bezi hallarda cedvel adina gore ve aliasa gore mucariet  mecburi olur
--misal olaraq eger biz bu iki amilden istifade etmesek bu sql xeta verecekdir.
--column ambiguously defined
select * from employees join departments  on department_id =department_id;

--Bu adda xeta onu bildirirk ki, ON bolmesinde yazdigimiz iki department_id
--sutunlarin adlari her iki cedvelde eynidir ve oracle bilmir ki beraberliyin
--sol ve sag terefinde olan bu sutunlar hansi hansi cedvele aiddir.
--bu sebebden de biz ya cedvel adini qeyd etmeliyik ya da alias yazmaliyiq.
--meselen
select * from employees join departments on employees.department_id=departments.department_id;

--alias bolmesini ozunuz yaziniz!

--column ambiguously defined xetasi tekce ON bolmesinde yazildiqda deyil,
--hemcinin select bolmesinde eyni adli sutunlar yazildiqda da bas verir
--misal olaraq
select department_id from employees join departments on employees.employee_id=departments.department_id;
--burada da qeyd olunan department_id sutunun hansi cedvele aid oldugunun bilmediyinden bu xetani verir


--INNER JOIN 
--emekdasa aid ad,soyad ve onun calisdigi departamentin adini  cixarmaq
select e.first_name,e.last_name,d.department_name from employees e 
 join departments d on e.department_id=e.department_id;
 
 --Yuxarida istenilen melumatlari yalniz Marketing dep. calisanlar  ucun cixarmaq
 --bunun ucun evvelki derslerde kecdiyimiz where operatorunu burada elave edeceyik
 select e.first_name,e.last_name, d.department_name from employees e
 join departments d on e.department_id=d.department_id 
  where d.department_name='Marketing';
  
 --hansi departamentlerde emek haqqisi 5000 den yuxari olan departament
 --adlarini ekrana cixarmaq
 select max(e.salary),d.department_name from employees e
  join departments d on e.department_id=d.department_id 
  group by d.department_name
  having max(e.salary)>5000;
  
 --INNER JOIN ferqli formatda da yazila biler
 --oracle in evvelki versiyalarinda olan bu formati indi de yazmaq mumkundur
 --bezi halllarda bu sekilde de yazanlar olur deye  bunu bilmek lazimdir
 
 select e.first_name,e.last_name,d.department_name 
  from employees e,departments d where e.department_id=d.department_id;

--bu halda JOIN  yazilmir ON bir basa WHERE ile evez olunur.

--Eger deyilse ki departament ve hemin dep. calisanlarin sayini 
--gosterecek siyahini hazirla o zaman INNER JOIN komeklik 
--etmyecek. beleki departamentler arasinda eleleri ola bilerik ki,
--oarada emekdaslar calismasin yeni employees cedvelinden ona aid melumat
--olmasin bele olan halda INNER JOIN bu kimi dep-lerin adini siyahida gostermeyecek.
--bu nov departamentlerin siyahida gorunmeyi ucun LEFT JOIN ve YA RIGHT JOIN den 
--istifade olunmalidir.
--misal olaraq ,
select  d.department_name,count(*) from employees e
 join departments d on  e.department_id = d.department_id
 group by d.department_name;

--11 sayda melumat cixardigi halda eslinde departments cedvelinde daha cox melumat var. 
--yoxlayaq. 27 sayda melumat var.
select * from departments;

--biz bu sayda departamentleri ekranda gormek ucun right join den istifade etmeliyik
select d.department_name,count(*) from employees e 
 right join departments d on  e.department_id=d.department_id
  group by d.department_name;
  
--right join yazmaq biz join sag terefinde olan cedveldeki butun setirleri 
--ekrana cixarir ve daha sonra onlara uygun saylar varsa hemin saylari qarsinina elave edirik.
--Eger departments cedvelin join in sol terefinde olsa o zaman left join yazmali idik
--mes
select d.department_name,count(*) from departments d 
 left join employees e on  e.department_id=d.department_id
  group by d.department_name;
  
--Diqqet etsek gorerik say bolmesinde her dep. uzre en azindan bir say gosterir.
--eger bu beledirse, o zaman deye bilerik ki, her dep aid en azindan bir isci var idise
-- o zaman bu INNER JOIN de niye az sayda melumat cixarirdi ..yani 12 dep evezine 
--27 cixarmali idi.  
--eslinde 12 sayda cixarmagi normaldi. burada esas mesele count(*) yanlis netice
--qaytarmasidir. Beleki count(*) null olan ifadeleri de deyer kimi sayir.
--mehz bu sebebden biz ekseriyyeinden count(*) deyer olaraq 1 deyerini goruruk.
--eger count(*) null da deyer kimi sayirsa o zaman biz count(*) yerine
-- count(e.employee_id) yazsaq dogru netice almis olacaqiq.

select d.department_name,count(e.employee_id) from departments d
 left join employees e on e.department_id=d.department_id
 group by d.department_name;



--non-equoin


/*

    The Salaries_Grades table:
        Grade: salary level.
        Min_Salary: minimum salary for a given level.
        Max_Salary: maximum salary for a given level.

    The Employees table:
        EmployeeID: primary key, ID of the employee.
        FirstName: first name of the employee.
        LastName: last name of the employee.
        Salary: salary of the employee.
        
        
        SELECT E.FirstName, E.LastName, S.Grade
FROM Employees E, Salaries_Grades S
WHERE E.Salary BETWEEN S.Min_Salary AND S.Max_Salary;



Grade	Min_Salary	Max_Salary
1	        1000	2000
2	        2001	3000
3	        3001	4000
4	        4001	5000



EmployeeID	FirstName	LastName	Salary
1	            John	Doe     	1500
2	            Jane	Doe	        2500
3	            Alice	Smith	    3500
4	            Bob	    Johnson	    4500
5	            Charlie	Brown	    5500




FirstName	LastName	Grade
    John	Doe	        1
    Jane	Doe     	2
    Alice	Smith	    3
    Bob	    Johnson	    4
    Charlie	Brown	    NULL

*/

--SELF JOIN

--misal olaraq, her emekdasin ad ve soyadi yaninda manager ad ve soyadini da gormek isteyirik bu zaman
--hem manager ham da employee uzre ad ve soyadin saxlandigi employees cedvelinin oz-ozune join edeceyik
--bunun ucun ilk once bilmeliyik ki, eyni adli 2 employees cedvelinde hansi employees aid olacaq, hansi ise managerslere
--biz tables uvun alias dan behs etmisdik. mehz bu nov ucun join de table alias lardan istifade etmek vacibdir.
--beleki table adlar eynidir ve bunlari necese ayirmaq lazimdir

/*
    select e.first_name,e.last_name ,m.first_name,m.last_name from employees e join employees m on e.manager_id=m.id;
   
*/ 

--natural joins


