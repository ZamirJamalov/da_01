--Constraints
--ER vizuallasdirma constraintlerden onceki veziyyeti
--İstifade sebebleri -  data integrity qorunmasi
--Novleri   -primary key, FK, not null, unique , check
--Statusun redaktesi enable/disable 
--Silinmesi  - drop 
--ER vizuallasdirma



drop table school;

CREATE TABLE SCHOOL( 
                        ID          NUMBER,
                        NAME        VARCHAR2(30),
                        ADDRESS     VARCHAR2(100)
                      );



drop table class;

CREATE TABLE CLASS(
                            ID          NUMBER,
                            SCHOOL_ID   NUMBER,
                            NAME        VARCHAR2(50)
                        );    



drop table teacher;

CREATE TABLE TEACHER(
                            ID          NUMBER,
                            NAME        VARCHAR2(30),
                            SURNAME     VARCHAR2(30),
                            BIRTHDATE   DATE
                          );


drop table class_teacher;

CREATE TABLE CLASS_TEACHER(
                                CLASS_ID          NUMBER,
                                TEACHER_ID        NUMBER
                              
                              );


drop table student;
 
CREATE TABLE STUDENT(
         
                              ID            NUMBER,
                              CLASS_ID      NUMBER,
                              NAME          VARCHAR2(30),
                              SURNAME       VARCHAR2(30),
                              BIRTHDATE     DATE
                            );
                            
                            
drop table mark;

CREATE TABLE MARK (
         
                             STUDENT_ID     NUMBER,
                             MARK           NUMBER,
                             MARK_DATE      DATE
                            );




----İstifade sebebleri 
--Entity integrity

insert into school(id,name,address) values(null,'Mekteb N10','Mahmudov kuc1');
commit;

select * from school;

insert into school(name,address) values('Mekteb N15','Cabbarov kuc4');
commit;

select * from school;

--entity integrity -ni hell etmek ucun primary key(unique ve not null) constraintini elave edek.
alter table school add constraint school_PK primary key(ID);

--burada constrainte ad vermede sutun adini yazmiriq ona gore ki,  bir cedvelde ancaq bir  PRIMARY KEY ola biler deye,
--sutun adini yazmaqa ehtiyyac qalmir

--88 setrdeki sorgunu icra etdikde xeta alacagiq , ona gore ki, school cedveline az once ID -si NULL olan 2 deyer elave etmisdik.
--Butun constraintlerde bir seyi nezere almaq lazimdir. Sutun uzre constraintlerin problemsiz yaradila bilmesi ucun,
--hemin constraint hemin sutunda hansi veziyyete nezaret edecekse, hemin veziyyeti constraint yaradilmamisdan once hemin sutunda 
--yaratmaq lazimdir. Yani, eger constraint sutunda NULL deyerinin olmamasini temin edirse, demek bu nov constraint elave edilecek
--sutunda daha oncede NULL deyeri olmamalidir.

--Bunun biz ilk once ID sutununda olan bu problemi  aradan qaldirmaq ucun tedbirler gormeliyik.
--1. NULL olan setrleri silmekle. 2. NULL olan sutunlarda ozumuzden deyerler yazmaqla

--2-ci variant uzre hereket ederek hemin sutunlara deyerler yazaq
--nezere alsaq ki, hemin cedvelde ancaq 2 setr melumat var  ,bu sekilde UPDATE yazaq .Eger melumatlar cox olarsa, ve onlarin hecde hamisinda
--NULL deyerler olmazsa , o zaman  bu SQL butun setrler uzre ID sutununda ki deyerleri 1 le evez edecekdir.
update school set id=1;
commit;
--neticeye baxaq
select * from school;

--artiq burada deyerlerin oldugunu goruruk ve constraintle bagli yazilan sorgunu bir daha icra edirik.
alter table school add constraint school_PK primary key(ID);

--BU defe de xeta verdi. Ancaq bu zaman xeta basqadir. Ve deyir ki, melumatlarda tekrarlanma var.
--Yadimizdadirsa PRIMARY KEY eyni zamanda hem sutunda NULL deyerlerin olmamasindan ve hemin sutunda deyerlerin unikal olmasindan
--sorumludur.
--Az onceki qeydimize esasen demek hem de unikalligi da daha once yaratmaq lazimdir ki, PRIMARY KEY  constrainti yaradila bilsin.
--unikalligi temin edek. Ikinci elave etdiyim mektebe ID 2 deyeri verek
update school set id=2 where name='Mekteb N15';
commit;
--constrainti bir daha icra edek;
alter table school add constraint school_PK primary key(ID);
--artiq constraint normal icra edildi. Sonda olan PK -PRIMARY KEY  anlamina gelir
--indi ise NULL ve ya tekrar deyer elave etmekle islediyinden emin olaq
--ID null elave edek
insert into school(name,address) values('Mektev N 50','Narimanov kuc2');
commit;
--Bu zaman ORA-01400: cannot insert NULL into ("DA_ZAMIR"."SCHOOL"."ID") seklinde xeta gorururuk. Bu o demekdir ki,
--PRIMARY KEY NULL olan deyerleri ID sutununa elave etmeye icaze vermir.

--tekrar deyerleri elave etmekle yoxlayaq
insert into school(id,name) values(1,'Mekteb N112');
commit;
--bu zaman ORA-00001: unique constraint (DA_ZAMIR.SCHOOL_PK) violated sekilde xeta goruruk. Bu o demekdir ki,
--PRIMARY KEY tekrarlanan deyerlerin ID sutununa elave etmeye icaze vermir.

--Bundan sonra bu iki adda xetalarla rastlashsaq bilin ki, PRIMARY KEY bagli olan sutuna ya NULL deyeri yada 
--hemin sutunda artiq movcud olan deyeri elave etmeye cehd edirsiniz.

--Eger xatirlayiriqsa PRIMARY KEY acarini cedvelin ID sutununa elave edirdik ve Cedveldeki ID sutunu ise
--ona gore  yaradirdiq ki, bu cedvele diger cedvellerden istinad(lar) olacaq.

--Bir halda ki, biz school cedveline ID sutunu elave etmisik , o zaman buna istinad edecek cedvele nezer salaq
--bizim halda bu cedvel CLASS cedvelidir.

--eger CLASS cedvelin ozune de diger cedvellerden istinad(lar) olarsa, o zaman SCHOOL cedveline elave etdiyimiz
--PRIMARY KEY constraintden elave edeceyik. 
--Indi ki halda ise bize maraqli olan hisse CLASS cedvelinin SCHOOL cedveline olan istinadidir.

--Entity integrity ile tanis oldur. Basqa bir nov ise  FOREIGN KEY ve ya REFERENTIAL integrity
--Burada esas meqsed istinad olunan deyer istinad olunan cedvelin aidiyyati sutununda her zaman olmalidir.

--misal olaraq, mektebe bagli bir sinif elave edek
select * from school;
select * from class;

--Misal olaraq, 15 N mektebe 5A sinifini elave edek.
--bunun ucun CLASS cedvelinde 5A sinifi 15 N mektebin ID deyerini saxlamalidir.
insert into class(ID,SCHOOL_ID,name) values(1,2,'5A');
commit;

--neticeye baxaq 
select * from class;

--bize deyirler ki, 15 N mekteb legv edilir ve onun adi cedvelden silinmelidir.
--bunun ucun hemin mektebin ID ne gore cedvelden silmek ucun, sorgu yazaq
delete from school  where ID=2;
commit;

--neticeye baxaq

select * from school;

--15 N mekteb artiq cedvelde yoxdur , amma class cedveline baxsaq goreruk ki, orada olan 5A sinifi artiq movcud olmayan mektebe baghlidir
select * from class;
--Bu hal az once dediyimiz  REFERENTIAL integrity nin pozulmasi halidir. 
--ve bele hallarin qarshisini almaq ucun FOREIGN KEY constraintden istifade edilir.
--class cedveline bu constrainti elave edek

alter table class add constraint CLASS_SCHOOL_FK FOREIGN KEY(SCHOOL_ID) REFERENCES SCHOOL(ID);

--xeta vermesinin sebebi SCHOOL_ID  olan deyerlerden hansininsa istinad etdiyi cedvelin sutunda tapmamasdir
--5A aya bagli id si 2 olan mekteb haqda SCHOOL cedvelinden melumat yoxdur.
--Burada 5A nin movcud olmasinin bir anlami yoxdur beleki bu sinif olan mekteb movcud deyil.
--buna gore de bu melumati CLASS cedvelinden silmek daga dogru olar

delete from class where name='5A';
commit;

--neticeni yoxlayaq
select * from class;
--melumat silinib. indi ise yeniden FOREIGN KEY  yaratmaga calisaq
alter table class add constraint CLASS_SCHOOL_FK FOREIGN KEY(SCHOOL_ID) REFERENCES SCHOOL(ID);

--constraint normal yarandi

--diger mektebe aid sinif elave edek
select * from school;

insert into class(id,school_id,name) values(1,1,'1A');
commit;

--neticeye baxaq
select * from class;

--eger biz bu halda mektebi SCHOOL cedvelinden silmeye calissaq 
delete from school where id=1;
commit;

--o zaman biz xeta almis olacagiq:   integrity constraint (DA_ZAMIR.CLASS_SCHOOL_FK) violated - child record found

--Yani silmek istediyimiz melumatdan asilli melumat tapilmisdir deye.

--Demek REFERENTIAL integrity -de olan telebi  FOREIGN KEY elave etmekle qorumus olduq


--Diger bir nov constraint UNIQUE -dir hansi sutundaki deyerlerin tekrarlanmasinin  qarsisini alir.
--artiq bilirk ki, bu  nezaret hemcinin PRIMARY KEY de var .
--amma biz bunu ayrica constraint kimi de istifade ede bilerik

--misal olaraq, SCHOOL cedvelinden mekteb adlarinin tekrarlanmamasi vacibdir ve bu sebebden biz NAME cedveline
--UNIQUE constraintini elave edek.
alter table school add constraint SCHOOL_NAME_UQ UNIQUE (NAME);

--terkarlanan mekteb adi daxil edek
select * from school;

insert into school(id,name,address) values(2,'Mekteb N10','Hamidov 4 kuc');
commit;

--bu zaman ORA-00001: unique constraint (DA_ZAMIR.SCHOOL_NAME_UQ) violated xetasini alacagiq.
--bu o demekdir ki, artiq biz hemin sutun uzre ancaq unikal deyerleri saxlaya bilerik.
--Amma bu constraint bizi mekteb adi olmadan setri SCHOOL cedveline elave etmekden qorumayacaq.
--bunun ucun biz mutleq olaraq NOT NULL constraintinden  istifade etmeliyikk.
--yadimizdadirsa biz demisdik ki, PRIMARY KEY bu iki constrainti ozunde cemleyir ve bele hallda bundan istidafe edirik.
--amma nezere alsaq ki, artiq PRIMARY KEY constraintini SCHOOL cedvelinin ID sutuna tetbiq etmisik, ve cedvelde ikinci bele bir
--PRIMARY KEY consraintinin olmasi mumkun deyil , biz bu halda mecburen NAME sutununa 2 sayda constraint elave etmeli olacagiq.
--UNIQUE ve NOT NULL 
--1-cini artiq elave etmisik , qalir NOT NULL
alter table school  MODIFY (NAME NOT NULL);
--bu nov constrainde artiq ad vermirik ce syntax bu sekilde olur.

--NAME e NULL deyeri olar setr elave etmeye calisaq

insert into school(id,name,address) values(2,NULL,'Arifzade kuc');
commit;

--Bu zaman ORA-01400: cannot insert NULL into ("DA_ZAMIR"."SCHOOL"."NAME") xetasini goreceyik


--USER -DEFINED integrity

--Ola biler ki, bizden melumatlarin elave edilmesini mueyyen yoxlamalarin elave edilmesi ve melumatlarin elave olunmasinda
--bu yoxlamalardan kecirerek bu melumatlari cedvelde yadda saxlamagi teleb ede bilerler
--bu zaman biz CHECK constraintinden istifade etmeliyik

--misal olaraq, bize deyiler  ki, mekteb adini yazanda mekteb adinin uzunlugunun en az 3 simvoldan ibaret olmasina nezaret etmek lazimdir
--nezere alsaq eyni bir sutuna ferqli sertler esasinda ferqli CHECK constrainti elave etmek olar
--o zaman bu constraintlerin bir-birinden ayirmaq ucun hemin CHECK leri yaratmaqda olan sebebi qisa sekilde adlandirmada 
--eks  etdirmek lazimdir ki, sondaran onlari vizual musahisdede ayirmaq mumkun olsun.
alter table school add constraint SCHOOL_NAME_MIN_LENGTH_CHK CHECK(length(name)>2);

--2 simvoldan ibaret mekteb adini elave etmeye cehd edek
insert into school(id,name,address) values(2,'aa', 'Dovlatov 1 kuc');
commit;

--bu zaman ORA-02290: check constraint (DA_ZAMIR.SCHOOL_NAME_MIN_LENGTH_CHK) violated xetasini goreceyik.
--bu o demekdir ki, name sutunna yalniz  3 ve daha cox  simvollu adlandirma elave ede bilerik


--Constraintlerin  enable ve disable edilmesi.
--bezen olur ki, mueyyen sebeblerden constaintleri  nezaretini muvveqeti saxlamaq lazim oluq.
--bunun ucun xususi emrler bunu etmek olar.
--PRIMARY KEY ucun  bu sekilde
alter table school disable constraint SCHOOL_PK;
--olmalidir. Amma PK ucun bir meqami nezere almaq lazimdir. PK lara her zaman FK lerden istinad olur. 
--ona gore bele hallarda ya 1 asagida ki emrler PK den asilli butun FK lari disable etmek olar , ya da  PK den asilli FK lari tapib
--onlari bir-bir disable etmek.
--1-ci varianta esasen yani FK leri disable etmeyi sistemin ohdeliyine buraxmaq isteyirikse o zaman
alter table school disable constraint SCHOOL_PK CASCADE; 
--yazmaq lazimdir. Bu zaman sistem  bu PK dan asilli butun FK lari taparaq DISABLE edecekdir. Yani qeyr aktiv hala salacaqdir ,
--hansi ki , hemin sistemin tapdigi FK -lar fealliye gostermeyecekler. Sistem bize demeyecek ki, hansilari DISABLE edib.
--mehz buna gore, yani bizim bilmediyimiz ucun ki, hansi FK lar sondurulub, onlari yeniden berpa etmek ucun gerek bir-bir 
--atararaq tapmaliyiq. Yani PK ni yeniden aktiv edende hemin FK-lar tekrar aktiv olmayacaqlar. Bunu gerek elave  emrle heyaya kecire bilerik.
--Nezere alsaq bizim halda SCHOOL-dan asilli yalniz bir CLASS cedveli var CASCADE bize ciddi problem yaratmayacaq
--Amma nezere alsaq ki boyuk sistemlerde cedveller arasi elaqeler daha coxdur ve cedvel sayi da az olmur, o zaman qeyd olunan sebebden
--CASCADE real is yerinde problemler yarada biler
--2-ci varianta esasen FK lar manual olaraq mueyyen ederek , ilk novbede hemin FK lari disabe edib, daha sonra 
--PK ni CASCADE  elave etmeden disable ede bilerik.
--teklif edirem , 2-ci variant uzerinden hell edek.
--bilirik ki, SCHOOL cedveline istinad eden bir sayda CLASS cedveli var. ve ilk once hemin cedveldeki FK -ni disable edek

alter table CLASS disable constraint CLASS_SCHOOL_FK;
--Ugurlu disable dan sonra  PK ni disable ede bilerik.
--amma nezere almaq lazimdir ki, CLASS_SCHOOL_FK nin disable olmasi zamani kimse SCHOOL cedvelinden ID deyeri 
--CLASS cedeveline bagli olan setri silerse, o zaman  hemin deyer olan sinif adi CLASS da  qalacaq amma school da olmayacaq
--yani naqadar ki CLASS_SCHOOL_FK  disable dir, o qeder zamana CLASS cedvelinden  REFERENTIAL integrity pozulubdur kimi basa dusmek olar.
--bu o demekdir ki, constraintleri DISABLE olunmasi ozunde melumatlarin tamliginin pozulmasina zemin yaratmis olur,
--eger bu disable olunma emri is zamani verilibse, adeten bu kimi isleri isden sonra , insanlarin sistemden istifade etmediyi hallarda
--edilirki, yeni melumatlarin elave edilmesi zamani oldugu halda dediyimiz tamligin pozulmasi halini olmasin deye

--PK da disable edek
alter table SCHOOL disable constraint SCHOOL_PK;
--SQL developerden her iki cedvele baxaq , gorek constraintler hansi veziyyetdediler
--bunun ucun her iki cedvelin constraint bolmesine baxmaq lazimdir
--STATUS bolmesinde DISABLED oldugunu goruruk. 
--qeyd olunan kimi, constraint ya DISABLED  halindadir ya da hech yoxdur hec bir ferq etmir. Melumat tamligi qorunmur.
--Biz hec zaman deye bilmerik ki, DISABLED olan halda melumatlarin tamligi qismen qorunur. Bu yanlisdir. Umumiyyetle qorunmur.
--Eyni zamanda CLASS da olan FK yada baxaq  
--Bu da hemcinin DISABLED statusundadir.
--PK ni aktivlesdirek ve baxaq gorek ki FK -da aktivlesir ya yox
alter table school enable constraint SCHOOL_PK;
--CLASS_SCHOOL_FK yoxladiqda gorerik ki, hele de disable statusundadir.
--onu da aktivlesdirek
alter table class enable constraint class_school_fk;


--Constraintlerin silinmesi DROP emri elave etmekle heyata kecirilir. enable yerine drop yazdiqda hemin adda constraint silinecek
--eger PK dirsa o zaman xeta verecek ondan asilli FK -lar olarsa, burada 2 yanasma olacaq
--1. CASCADE elave etmekle sistem ozu butun FK lari tapib silecek
--2. ya da ozumuz onlari tapib uygundursa ayrica silerek daha sonra PK ni sileceyik.

--Diger cedveller uzre esas olan iki PRIMARY KEY ve FOREIGN KEY leri elave edib vizual olaraq cedvellerin elaqesine baxaq

--class ucen PK elave edek
alter table class add constraint CLASS_PK primary key(id);
--muellim ucun PK elave edek
alter table teacher add constraint teacher_pk primary key(ID);


--class_teacher ucun 2 FK elave edek .
--burada artiq FK ad verende hem de sutun adini elave edeceyik , beleki bir cedvelde 2 FK olacaq deye onlarin bir-birinden ayirmaq ucun
alter table class_teacher add constraint class_teacher_CLASS_ID foreign key(class_id) references CLASS(ID);
alter table class_teacher add constraint class_teacher_TEACHER_ID foreign key(teacher_id) references TEACHER(ID);

--student cedveline hem PK hem de CLASS ucun FK elave edek
alter table student add constraint STUDENT_PK  primary key(ID);
alter table student add constraint STUDENT_CLASS_FK foreign key(class_id) references class(id);

--mart cedveline student e FK elave edek
alter table mark add constraint mark_student_FK foreign key(student_id) references  student(id);

--ER vizuallasmaya baxaq


--Constraintleri cedvelleri yaradan zaman tetbiq etmek de mumkundur.
--ilk once cedvelleri silek 

--yeni DLL leri yaradaq harada ki constraintlerde elave olunubdur.
--bu zaman DDL lerin yaradilmasinda mueyyen ardicilligin olmasi sertdir.
--misal olaraq class cedvelini school cedveline olan FK ile birge  hemin school cedvelinden once yaratmaga cehd etsek,
--xeta alacagiq ona gore ki, class cedvelinin istinad etdiyi school cedveli hele movcud deyil.
--ona gore de mutleq olaraq, bunlari nezere alib ardicilligi mueyyen etmek lazimdir.

--ilk once school  table yaradiriq ona gore ki, bu cedvel diger cedvellere istinad etmir
--burada elave edeceyimiz constraintler
--ID primary key
--name not null 
--name check min 3 simvol
CREATE TABLE SCHOOL( 
                        ID          NUMBER,
                        NAME        VARCHAR2(30) not null,
                        ADDRESS     VARCHAR2(100),
                        constraint SCHOOL_PK primary key(id),
                        constraint  SCHOOL_NAME_MIN_LENGTH_CHK CHECK(length(name)>2)
                      );
                      
 --cedvel uzerinden neticeye baxaq  
 
 
--class cedvelini DDL hazirlayaq
--id primary key
--name not null
--school id FK
CREATE TABLE CLASS(
                            ID          NUMBER,
                            SCHOOL_ID   NUMBER,
                            NAME        VARCHAR2(50) not null,
                            constraint class_pk primary key(id),
                            constraint class_school_fk foreign key(school_id) references school(id)
                            
                        );    
--neticeni cedvel uzerinden yoxlayaq . cedvelleri refresh edek , class cedveli siyahida gorunsun

--teacher cedveli uzun DDL hazir edek
--ID primary key
--name not null,
--surname not null,
--birthdate check 1930 dan boyuk  2020 den ise kicik olsun

--tarixe gore bu sekilde mehdudiyyet tetbiq etmek dogru deyil dir, cunki zamanla 2020 serheddi yasa dolduqca surusmelidir
--amma bizim halda bu deyer statik olaraq her zaman qalacaq ve mueyyen illerden sonra cedvelin  bu constraintinde
--bu surusmeni redakte etmek lazim olacaq. adeten bele nov yoxlamalari cedvellerde constraintlerle deyil,
--triggerle edirler , harada ki, biz deye bilerik ki, son serheddi cari zamana gore mueyyen et.
--triggerlerleri novbeti derselerde oyreneceyik plsql bolumunde
--yani texniki olaraq, mumkundu, biznes olaraq da bugun ucun ok-di,amma yaxin gelecekde problemler yarada biler deyeyene
--bunu ferqli usulla triggerlerle etmek tovsiyye olunur

CREATE TABLE TEACHER(
                            ID          NUMBER,
                            NAME        VARCHAR2(30) not null,
                            SURNAME     VARCHAR2(30) not null,
                            BIRTHDATE   DATE,
                            constraint teacher_pk primary key(ID),
                            constraint teacher_birthdate_validation_CHK 
                                check(birthdate>=to_date('01/01/1930','MM/DD/YYYY') and birthdate<to_date('01/01/2020','MM/DD/YYYY'))
                          );


--class teacher ucun DDL hazir edek
--class_id FK
--teacher_id  FK
CREATE TABLE CLASS_TEACHER(
                                CLASS_ID          NUMBER,
                                TEACHER_ID        NUMBER,
                                constraint CLASS_TEACHER_CLASS_ID_FK  foreign key(class_id) references class(id),
                                constraint CLASS_TEACHER_TEACHER_ID_FK  foreign key(teacher_id) references teacher(id)
                              );


--student ucun DDL hazir edek
--id primary key
--class_id FK
--name not null
--surname not null
--birthdate check 
 
CREATE TABLE STUDENT(
         
                              ID            NUMBER,
                              CLASS_ID      NUMBER,
                              NAME          VARCHAR2(30) not null,
                              SURNAME       VARCHAR2(30) not null,
                              BIRTHDATE     DATE,
                              constraint student_pk primary key(id),
                              constraint student_class_FK foreign key(class_id) references  class(id),
                              constraint student_birhtdate_valudation_chk
                                 check(birthdate>=to_date('01/01/2010','MM/DD/YYYY') and birthdate<to_date('01/01/2020','MM/DD/YYYY'))
                            );
                            
                            
--mark ucun DDL yaradaq

--student ID primary key 
--mark 2,3,4,5  check 
--mark_date  not null
CREATE TABLE MARK (
                             STUDENT_ID     NUMBER,
                             MARK           NUMBER,
                             MARK_DATE      DATE not null,
                             constraint mark_student_fk foreign key(student_id) references student(id),
                             constraint  mark_mark_validation_chk check( mark in (2,3,4,5))
                             
                            );

--ER vizuallasdirmaya yeniden baxaq

