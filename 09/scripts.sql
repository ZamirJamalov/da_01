--DML 

--insert 
--dogru hal 
insert into school(id,name,address) values(2,'10 nomreli mekteb','Valizade 1');
commit;

--dogru hal 
insert into school(id,name) values (3,'89 nomreli mekteb');
commit;

--dogru deyil syntax xeta
insert into school(id,name)  (4,'90 nomreli mekteb');
commit;

--dogru deyil semantic xeta 
insert into school(id,mame) values(5,'14 nomreli mekteb');
commit;

--dogru deyil biznes xetasi
insert into school(id,name) values(2,'17 nomreli mekteb');
commit;

--dogru deyil biznes xetasi 
insert into school(id,name,address) values(6,'Hamidov A 1','34 sayli orta mekteb');
commit;


--dogru hal
insert into school values(7,'43 nomreli mekteb', 'Ahmadzade Y 19');
commit;

--dorgu deyil syntax xetasi
insert into school values(7,'54 nomreli mekteb');
commit;

--P.S Daxil etdiyimiz melumatlari deyismek ucun INSERT istifade olunmur!  Eger her hansi melumati duzelis etmek isteyirikse
--INSERT istifade olunmamalidir! Her INSERT zamani  Table yeni setr elave edilir. Cari melumatlari redakte etmek ucun YALNIZ
--UPDATE emrinden istifade etmeliyik.

--update 
--evvelki melumatlara baxmaq
select * from school where id=1;
--Adi redakte etmek 
update school set name='1 sayli orta MEKTEB' where id=1;
commit;

--dogru deyil 
update school set name='1 sayli orta MEKTEB';
commit;

--dogru hal
update school set name ='1 sayli orta mekteb',address='Ahmadzade 5' where id=1;
commit;



--sual
--nece sayda setr redakte olunacaq
update school set name='18 nomreli mekteb' where 1=2;
commit;


--DELETE 
--dogru hal 
delete from school where id=7;
commit;

--dogru deyil
delete from school;
commit;

--TRUNCATE  commite ehtiyyac yoxdur cunki truncate DDL emelyyati salyilir
truncate table school;


--Constraintler
--Tek sutun uzre
--Unikalliq  
alter table school add constraint school_id_uq unique (ID);
--tekrar melumat elave etmeye cehd edek
insert into school(id) values(1);
commit;
--redakte etmekle tekrar melumat yaratmaqa calisaq
insert into school(id,name) values(2,'17 nomreli mekteb');
commit;
--
update  school set id=1 where id=2;
commit;

--NOT NULL  
--
insert into school(name,address) values('32 nomreli mekteb','Abbasov');
commit;
--
select* from school;
-- NOT null constraintini  indi tetbiq etsek xeta verecek.
alter table school modify (id not null);
--xetanin sebebi ID cedvelinde null deyeri olmasidir . Bu o demekdir ki, constraintleri tetbiq etdiyimiz zamani
--hemin sutunlar uzre mehdudiyyetin odenilmesi telebleri onceden odenilmelidir. Bu hemcinin daha evvel
--istifade etdiyimiz  UNIQUE de aiddir.
--bu veziyyeti duzeltmek ucun ya problem yaradan setri tam silmek lazimdir ya da hemin xana uzre null deyerini ferqli deyerle
--evez etmek lazimdir.

--
select  * from school;

--redakte edek XETA verecek;
update school set  id=3 where id=NULL;
commit;

--redakte edek 
update school set id=3 where id is null;
commit;

--bir daha constraint elave etmeye cehd edek
alter table school modify (id not null);

--bir daha null deyeri elave etmeye cehd edek
insert into school(id) values(null);
commit;

--NOT NULL constraintini oracle ozu adlandirdi. hansi constraintle aciq sekilde ad vermirikse oracle hemin constraintler ozune uyhun
--sekilde adlandirir.

--constraintleri silek
--bunun ucun onlarin adlarini elde etmek lazimdir.
alter table school drop constraint SYS_C0053164;
alter table school drop constraint SCHOOL_ID_UQ;


--Foreign Key 
--bu acarin yadilmasi ucun gerek istinad olunan cedvelin adiyyati sutunu primary key ve ya unique  olsun
--misal olaraq, class cedvelindeki school_id sutununa foreign key constrainti elave edilmelidir.
alter table class add constraint CLASS_SCHOOL_ID FOREIGN KEY(school_id)  references  school(id);
-- problemi aradan qaldiraq ve yeniden 135 ci setri icra edek
alter table school add constraint school_PK primary key(id);

--multi unique constraints
alter table teacher  add constraint teacher_name_surname_uq unique(name,surname);
insert into teacher(name,surname) values('A','B');


--
select * from school;
