create table restoran
(ad varchar2(100),
 unvan varchar2(100),
 id number);
 /
 create table musteri
 (ad varchar2(100),
  odenis_id number
 );
 /
 create table yemek(
 ad varchar2(100),
 qiymet number(10),
 metbex_id  number
 );
 /
 create table odenis
 (tarix date,
  say number,
  mebleg number,
  yemek_id number);

/
create table metbex
(ad varchar2(100),
 restoran_id number,
 id number);
 /
 
 insert into restoran(ad,unvan,id) values('R1','Unvan1',1);
 insert into restoran(ad,unvan,id) values ('R2','Unvan',2);
 
 /


 insert into metbex(ad,restoran_id,id) values('isti',1,1);
 insert into metbex(ad,restoran_id,id) values('soyuq',1,2);
 insert into metbex(ad,restoran_id,id) values('isti',2,3);
 /
 
 insert into yemek(ad,qiymet,metbex_id) values('dolma',10,1);
 insert into yemek(ad,qiymet,metbex_id) values('mimoza',5,2);
 
 /
 insert into odenis(tarix,say,mebleg,yemek_id) values(trunc(sysdate)-2,1,10,1);
 insert into odenis(tarix,say,mebleg,yemek_id) values(trunc(sysdate)-2,1,5,2);
 
 /
 insert into musteri(ad,odenis_id) values('M1',1);
 insert into musteri(ad,odenis_id) values('M1',2);
 
 


 
 --Redundancy elametleri 
 --1. Bir musteri muxtelif odenisler etse, hemin musterinin adi -musteri cedvelinde tekrarlanacaqdir. 
select * from musteri;

 --Bu problemi aradan qaldirmaq lazimdir. Burada olan  xetamiz neyin -neden asilli oldugunu ters etmeyimizdir. 
 --Yani Musteride odenis_id nin deyil , odenisde musteri id sinin olmasi daha dogrudur.
 --Bunun ucun hem musteri cedvelinde hem de ki odenis cedvelerin yeniden yaradib melumatlari elave edek.
 drop table musteri;
 drop table odenis;
 /
 
 create table musteri
 (ad varchar2(100),
  id number --,
   --odenis_id number
 );
 /
 
 create table odenis
 (tarix date,
  say number,
  mebleg number,
  yemek_id number,
  musteri_id number);

/
 

 insert into musteri(ad,id) values('M1',1);
 insert into musteri(ad,id) values('M2',2);


 /
 insert into odenis(tarix,say,mebleg,yemek_id,musteri_id) values(trunc(sysdate)-2,1,10,1,1);
 insert into odenis(tarix,say,mebleg,yemek_id,musteri_id) values(trunc(sysdate)-2,1,5,2,1);
 /
 
 
 --her  yeni odenisde musterinin adi cedvelde tekrarlanmir. her yeni odenisde musterinin ID-ne istinad olunur.
 
 --Redundancy elameti.
 --2. Yemek adlarinin metbex-lere gore tekrarlanmasi ehtimali. Meselen. Dolma hem metbex 1 de ola biler hem de mebtex 2.
 --Bu hali ilk once yaraq. Bunun ucun eyni adli yemeyi  cedvele elave edek ve melumatlara yeniden baxaq.
 
 insert into yemek(ad,qiymet,metbex_id) values('dolma',9,2);
 
 select * from yemek;
 
 --Burada dolmani  tekrar olaraq goruruk.
 
--Bunu biz tersden etsek, yani metbex cedvelinde X metbex ucun ferqli yemekler cedvele elave etdikce, X metbexin adi her zaman tekrarlanacaq.
--Yani bu yol dogru deyil
--Basqa bir yol yemek ve metbexleri bir-birinde deyil, 3-cu cedvelde elaqelendirmek.
--Yani biz bu meselede de ,problemin helli ucun ER 3 tipini her defe sinayiriq.
--3-cu cedveli Sifarisler cedveli deye adlandiraq  ve bezi cedvellerdeki  sutunlari burada qeyd edek.
--ilk novbede cedveli yaradaq
create table sifarisler
(yemek_id number,
 muster_id number);
 
 --daha sonra "yemek" cedvelini bu yeni cedvele gore yeniden yaratmaq lazimdir.
 --yemek cedvelinde metbex_id -ni silmekle mesele tam hell olunmur, teessuf ki, buna sebeb olan meqamlari qeyd edecem.
 /
 drop table yemek;
 /
 create table yemek(ad varchar2(100),
                    qiymet varchar2(100),
                    id number);
 /

--yeni melumatlar daxil edek.
/
insert into yemek(ad,qiymet,id) values('dolma',10,1);
insert into yemek(ad,qiymet,id) values('mimoza',5,2);
/

--melumatlara baxaq.
select * from yemek;

--burada hansi yemayin hansi metbexe aid oldugu gorunmur. ve eger ferqli metbexler eyni adli "dolma" yemeyini hazirlayarlarsa, ve hemin yemeklerde qiymet ferqi olarsa, bu 
--cedvelete 2 eyni adli yemeyi elave etmekle yene de melumat artiqligini elde edeceyik, Bunun ucun burada, redundancy yaradan amili kenarlasdirmaq  ve 
--yeni cedvelde bunu nezere almaq lazimdir. bu sebebden de biz  yemek_qiymet cedvelini yaratmaliyiq.

create table yemek_cedveli
(yemek_id number,
 qiymet number,
 metbex_id number);
 
--bu deyisikliye gore "yemek" cedvelini yeniden yaratmaq lazimdir , harada ki, qiymet sutunu olmayacaq.
/
drop table yemek;
/
create table yemek(
  ad varchar2(100),
  id number
);
 /
 insert into yemek(ad,id) values('dolma',1);
 insert into yemek(ad,id) values('mimoza',2);
 /
 
--  bize deseler ki, ferqli metbexlere ferqli "dolma" adli yemeyi elave et ve her birinin ferqli qiymeti olsun
-- bunu yemek adlarini cedvelde tekrar daxil etmeden, melumatlari yemek_cedveline daxil ederek, ede bileceyik.
insert into yemek_cedveli(yemek_id,qiymet,metbex_id) values(1,10,1);
insert into yemek_cedveli(yemek_id,qiymet,metbex_id) values(1,9,2);


--
select * from yemek_cedveli;

--her defe yeni yemekler elave etdikde  "yemek" cedvelinde  evvelki kimi,  elave deyerler artirmaqa ehtiyyac qalmir.


--Neticede urek acmayan bezi meqamlar qalib hansi ki, onlari da hell etmek gerekdir.

--Sifarisler cedvelin sifarislerin hansisa amile gore qruplasmalidir. Misal ucun sifaris nomresine gore.
--Diger meqam ise sanki odenis  ve sifaris cedvellerinde 2 eyni adli sutunlar yani yemek_adi ve musteri_id  eyni mentiqi tekrarlayirlar.
--bu sebebden biz nezere alsaq ki, daha once sifaris olunur yemekler, sonra bu sifarise gore odenis edilir.
--yaxsi olardi ki, odenis  cedvelinde  sifaris cedveline elave edeceyimiz sifaris nomresini sutuna istinad eden sutun elave edek, hansi
--odenis hansi sifaris nomresine gore edildiyi bilinecek, bu sifaris nomresi esasinda hansi mehsulu hansi musteri sifaris etdiyini sifarisler cedvelinden
--elde etmek mumkun oldugunu nezere alsaq, odenis cedvelinde yemek_id ve musteri _id sutunlarini saxlamaq ehtiyyac qalmir. beleki bu deyerleri ,
--sifaris nomresi vaasitesile sifarisler cedvelinden tapmaq mumkun olacaq.
 
 
 --sonuncu olaraq, bir nece meqami bir yerde elave etmek isteyirem,
 --sifarisler cedveline odenis cedvelindeki say sutunu ve elecede mebleg sutununu yeni adda qiymet adinda yeni sutunlar elave edirem
 --Say sifarise daha yaxindir ve bu sebebden sifarisler cedveline elave etdim.
 --qiymet sutunu da elave etdim baxmayaraq ki, bu adda sutun yemek_cedvelinde movcud idi. Buna sebeb evvelki tarixlerdeki edilen sifarislerin tarixde qalmasi ucundur.
 --yani  eger dolma-ni 10 azn bugun sifaris etdimse, sabah dolmani qiymeti 15 azn olanda,  sifarisler cedvelinden menim bu dolmani 10 azn aldigim gorunmelidir. 
 --baxmyaraq ki, bugun onun qiymeti 15 azn -dir. 
 --Bu sifaris ne zaman edilibse, hemin  gun yeni adli tarix sutununda saxlanilmalidir
 
 --odenis ve sifarisler cedvelini yeniden yaradaq
 
 /
 
 drop table odenis;
 drop table sifarisler;
 
 /
 create table odenis(
   tarix date,
   mebleg number,
   sifaris_nomre number
 );
 
 
/

create table sifarisler(
  yemek_id number,
  musteri_id number,
  say number,
  qiymet number,
  tarix date,
  nomre number
);

/

insert into  sifarisler(yemek_id,musteri_id,say,qiymet,tarix,nomre) values(1,1,1,10,trunc(sysdate)-1,2008000);
insert into sifarisler(yemek_id,musteri_id,say,qiymet,tarix,nomre) values(2,1,1,5,trunc(sysdate)-1,2008000);

/
insert into odenis(tarix,mebleg,sifaris_nomre) values(trunc(sysdate)-1,15,2008000);

/
select * from odenis; 

/

select * from sifarisler;
 
 
 
 

