/*
 window functions 
 ---------------------------
 group by  melumat coxlugunu aggregate etdiyi ucun  group by neticesinde  setrlerin her biri yekunda eks olunmadigi ucun bu setrlerin aggragete emeliyyatdan sonra gorunmesi ucun 
 window functions -lar tetbiq edilir.
 
 Asagidaki meseleni sadece group by komeyile hell etmeye calisaq:
 
 Employees cedvelinde olan emekdaslarin emekhaqqilarinin  calisdiqlari departament uzre max emek haaqi ile  muqayise edib ferqi tapmaq.
 
 Bundan evvelki derslerde max emekhaqqinin tapilmasi ucun bildiyimiz kimi aggregate funksiyalar istifade olunmalidir:
 
 Eger max salary -ni butun   employees  ucun tapmaq istesek o zaman :
 select max(salary) from employees;
 yazmaq lazimdir.
 Eger biz max salaryni dep-ler uzre almaq istesek o zaman  group by istifade olunmalidir.
 select department_id,max(salary) from employees group by  department_id;
 bu zaman artiq max(salary) evvelki deyeri deyil, mehz employees de olan 
 setrlerin department_id e gore qruplasmasindan yaranan ayri-ayri bolmelerdeki 
 setrler uzre max(salary)  hesablayib qaytarir. 

Umimi verilen tapsirigin bir hissesi olan yuxarida yazilan sorgu daha sonra
yazacagimiz sorgunun terkib hissesine daxil  olunacaqdir. 

 select e.*,e1.max_sal from employees e left join 
       (select department_id,max(salary) as max_sal from employees group by department_id) e1 
 on e.department_id=e1.department_id;
 
 Daha sonra qruplasma neticesinde elde etidiymiz  dep-ler uzre max salary deyerini her 
 setirdeki salary deyeri uzre riyazi emeliyyatda istifa ede ederek faiz nisbetini elde ede bilerik.
 Burada biz her emekdasin dep uzre max emek haqqinin nece faizinde emek haqqi alir, bunu teyin edeceyik.
 
  select e.*,e1.max_sal,(e.salary*100)/e1.max_sal from employees e left join 
       (select department_id,max(salary) as max_sal from employees group by department_id) e1 
 on e.department_id=e1.department_id;
 
 Burada biz  neticeni yuvarlaqlasdirmaq ucun round func-lardan istifade etmeliyik.
 
  select e.*,e1.max_sal,round((e.salary*100)/e1.max_sal) from employees e left join 
       (select department_id,max(salary) as max_sal from employees group by department_id) e1 
 on e.department_id=e1.department_id;
 
 Eger deyerin yaninda faiz isaresi elave etmek isyerikse o zaman  concat istifade etmeliyik
 
  select e.*,e1.max_sal,round((e.salary*100)/e1.max_sal)||'%' as sal_percent from employees e left join 
       (select department_id,max(salary) as max_sal from employees group by department_id) e1 
 on e.department_id=e1.department_id;
 
 Buradan gorunduyu  kimi verilen taskin icrasi ucun biz eyni cedvele 2 defe muraciet etmek zeruretinden olduq.
 
 Bu da performance baximindan elverisli sayilmir. Bunun ucun window func istifade etmek daha dogrudur.
 
 az once yazilanlari window functions vasitesile etmeye calisaq. Amma ilk olaraq, window funs- qurulusuhaqda 
 melumat elde edek. 
 
  select sum(salary) over() from employees e;
  
  bu sorgu eyni quvvelidi bu sorgu ile:
  
  select sum(salary) from employees; 
  
  select sum(salary) over(department_id) from employees;
  
  bu sorgu eyni quvvelidir 
  select sum(salary) from employees group by department_id; 
  
  ancaq  elde olunan netice ferqlidir. Beleki select sum(salary) from employees group by department_id sorgusu bize department_id uzre melumatlar toplularinin her biri 
  uzre max salary deyerini qaytardigi halda, select sum(salary) over(department_id) from employees sorgusu  cedveldeki butun setrleri qaytarmaqlaqla yanasi hemcinin her setrin qarsisinda elave 
  olaraq, butun departamentler uzre hesablanmis umumi salary deyerini qaytarir. 
  Eslin over(department_id) ve group by department_id oxsar olsa da her iki sorguda istifade olunan sum(salary)  over(department_id) hissesinde  department_id gore hesablanaan umumi deyeri 
  cedveldeki uygun butun setrle samil edir, group by department_id bolmesinde istifade olunan sum(salary) ise  ozunu ferqli apararaq  department_id uzre qruplasdirmada olan setrlere gore 
  hesablanib sadece hesablanmis deyeri qaytarir, over(partition by department_id) de oldugu kimi butun setrleri de elave olaraq qaytarmir. 
  Mehz bu sebebdendir ki, group by da istifade etdiyimiz aggreagate funksiyalar over() le islendikde window funs-lar adlanirlar beleki 
  over() adi cekilen cedvelde  ona verilen sutun adlarina gore pencereler yaradir ve bu sorgularda yazilan aggregate funksiyalar her penceredeki setrlere samil olunur. 
  Eger over() yazib moterize daxilinde  sutun adini qeyd etmesek, o zaman pencere cedvelin qaytardigini butun setrleri ehate edir. 
  
  Az once qroup by la yazdigimiz sorgunu window func yazmaga calisaq.
  
   select a.*,round(a.salary*100/a.max_sal)||'%' as sal_per from (
  select e.*,max(salary) over(partition by department_id) as max_sal from employees e
  ) a;
  
  Gorunduyu kimi, window funcs- komeyile employees cedveline bir defe muraciet etmis olduq. bu da performance gostericilerine diger sorgudan daha tez zamanda icra olunacaqdir.
  
  Burada ic-ice sorgu yazmaq sebebi odur ki, max(salary) over(partition by department_id) nin qaytardigi deyeri hemin anda istifade etmek mumkun olmadigindan, 
  bu sekilde sql yazilib. Yani max(salary) over(partition by department_id) qaytarigi deyerlerin uzerinden riyazi emeliyyatimizi apaririq.
  
  Eger meselenin serti deyiserse, ve bizden her emekdasin salary-sini eyni zamanda hem calisdigi department uzre olan max salary ile hem de
  butun emekdaslarin  salary uzre max salary ile muqayisesi istenilse o zaman group by la bunu etmeye calissaq elave olaraq 3 -cu defe 
  employees muraciet etmek lazim olacaqdir.
  
   select e.*,e1.max_sal,round((e.salary*100)/e1.max_sal)||'%' as sal_percent, round((e.salary*100)/(select max(salary) from employees))||'%' as sal_percent2
   from employees e left join 
       (select department_id,max(salary) as max_sal from employees group by department_id) e1 
 on e.department_id=e1.department_id;
 
 Ancaq, window func-s tetbiq etsek, bu halda da employees cedveline bir defe muraciet etmis olacagiq.
 
     select a.*,round(a.salary*100/a.max_sal)||'%' as sal_per,round(a.salary*100/a.max_sal2)||'%' as sal_per2 from (
  select e.*,max(salary) over(partition by department_id) as max_sal,max(salary) over() as max_sal2 from employees e
  ) a order by a.employee_id;
  
  Burada elave olaraq, butun emekdaslar uzre max salary tapmaq ucun  max(salary) over() elave etdik haradki over() cedvelin butun setrlerini ehate etdiyinden max(salary) butun setrler uzre uygun
  deyeri  her setr ucun qaytardi.
  
  
  Window functionslar umimiyyetler 3 tipe bolunurler.
  1. Aggregate funcs: avg,max,min,sum,count
  2. Ranking funcs: row_number,rank,dense_rank,percent_rank,ntile
  3. Value funcs: lag,lead,first_value,last_value,nth_value
  
  
  
  Ranking funcs:
  1. row_number 
   bu haqda danismamisdan once  analitik funskiyalara qeder movcud olan olan rownum haqda melumat alaq.
   beleki rownum  sorgunun neticesinde qaytarilan setrlere  nomre verir. misal olaraq,
  
   select rownum,e.* from employees e;
   
    baxmayaraq ki, neticede butun setrler artan sira ile nomrelenibdir ,lakin her zaman nomreleme ucun rownum istifade etmek mumkun olmur.
    beleki rownum nomrelenmeni order by dan evvel icra edir bu o demekdir ki, eger biz  order by istifade olunan sorguda rownum istifade etsek
    netice setrler artan sirada nomrelenmeyecekler.
    misal olaraq,
    
    select rownum,e.* from employees e order by e.first_name; 
    
    qeyd olunan kimi burada  rownum  order by dan evvel icra olundugu ucun ve daha sonra order by icra edildiyinden 22 setrde yer alan Adam order by gore birinci yerde gorunduyunden 
    bu neticeni elde etmis oluruq.
    
    eger her bir rownumber in duzgun calismasini isteyirikse o zaman rownum order by dan sonra icra edilmesini temin etmek lazimdir. misal olaraq,
    
    select rownum,a.* from (select * from employees order by first_name) a;
    
    eger sorgunu qisa yazmaq isteyirik se o zaman window funs- aid row_number istifade oluna biler.
    
     select row_number() over(order by e.first_name),e.* from employees e;
     
     burada order bu bolumunu over daxilinde yaziriq, qaydalara uygun olaraq, beleki, row_number over bize teqdim etdiyi  pencereye samil olundugdan,
     row_number istifadesinde over-de order by qeyd olunmasi mutleq lazimdir.
     
     ola biler ki, biz siralanmani ancaq departamentlerin terkibine uygun etmek istesek, yani her dep-de calisan emekdas 1-den baslayaraq nomrelensin bu zaman
     biz over uzre partition tetbiq ederek dep uzre melumatlar pencerelere bolunecek ve over-deki order by her pencereye ayrica samil edilecekdir. misal olaraq,
     
      select row_number() over(partition by department_id order by e.first_name),e.* from employees e;
      
      2. rank ve dens_rank
      
      mueyyen  deyerlere esasen setrlere ceki vermek ucun rank func-dan istifade olunur.
      misal olaraq,
      
      select rank() over(order by e.salary),e.* from employees e;
      
      neticede  elde olunan  cekilerin istifadesi yararsiz oldugu ucu daha dogru olar ki, en az ceki en boyuk salary samil olunsun ki, serhedleri mueyyenlesdirmek ve daha sonra avtomatlasdirmaq 
      olsun.
      bunun ucun siralamani tersden etmek kifayetdir.
      
      select rank() over(order by e.salary desc),e.* from employees e;
      
      bu neticeye esasen  misal olaraq, teyin etmek olar ki, en yuksek emekki alan ilk 10 neferi mueyyen edek 
      oracle da topN fuksiyasi olmadigi ucun bunu rank vasitesile edirler.
      
      select * from (select rank() over(order by e.salary desc) as rnk,e.* from employees e) a where a.rnk<=10;
      
      diqqet etsek gorerik ki, rnk -tekrarlanan nomreler var. bunun olmasina sebeb Rank  pencere terkibinde eyni deyerlere eyni cekini verir.
      Bundan elave olaraq, novbeti cekini verdikde evvelki setr uzre tekrarlanmalar olarsa, o tekrarlanma sayi qeder ceki nomresini buraxmis olur.
      eger bu buraxilmalarini istemirikse ve nomrelenmelerin ardicil olaraq  gelmesini isteyirikse, o zaman dense_rank dan istifade ede bilerik. 
      
      select * from (select dense_rank() over(order by e.salary desc) as rnk,e.* from employees e) a where a.rnk<=10;
     
      Eger task olaraq deyilerse ki, her department uzre en yuksek slaary alan 2 neferi goster o zaman
       
      select * from (select dense_rank() over(partition by e.department_id order by e.salary desc) as rnk,e.* from employees e) a where a.rnk<=2;
      
      3. percent_rank 
      rank -dan ferqli olaraq, setrlere cekini faiz nisbetinde verir. misal olaraq,
      
      select percent_rank() over(order by e.salary), e.* from employees e;
      
      pencerenin 1-ci setri her zaman 0 ,sonuncu setri ise her zaman 1 deyerini alir. 
      
      eger deyerleri yuvarlaq etsek o zaman daha aydin netice elde ederik.
      
      select round(percent_rank() over(order by e.salary),2)*100, e.* from employees e;
      
      neticede deyerler faiz nisbetinde oludugu ucun, bunun vasitesile bezi tasklari helle ede bilerik.
      misal olaraq,
      
      salary-si max -salarinin 80% -den yuxari teskil eden emekdaslarin siyahisi
      
      select * from (select round(percent_rank() over(order by e.salary),2)*100 pr, e.* from employees e) a where pr>=80;
      
      percent_rank hesablanma formulasi beledir:
      
      (rank-1)/(total_rows -1)
      
      4. ntile
      sorgunun qaytardigi neticeleri  qruplara bolmek ucun tetbiq olunur. 
      misal olaraq, emekdaslarin salary-leri uzre melumatlari 4 sayda qrupa bolmek istesek, bu sorgu yazilmalidir.
      
      select ntile(4) over(order by e.salary),e.* from employees e;
      
      Eger her dep uzre melumatlari qruplasdirib onlardan yalniz orta qrupa aid olanlari elde etmek istesek o zaman 
      sorgu bu sekilde yazilmalidir.
      
     select * from (select ntile(3) over( partition by e.department_id order by e.salary) as nt,e.* from employees e) a where a.nt=2;
     
      
 */
