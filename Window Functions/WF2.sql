--Window Funcs
--Value Types

--Lag 
--Bir onceki setrde ki deyeri goturur
--Misal olaraq, departamentler uzre setrler uzre evvelki emp salary uzre ferqi tapmaq lazimdirsa LAG istifade edeceyik
--LAG func-i 3 hisseden ibaretdir 1. Deyeri oxunacaq SUTUN adi 2. Geri oxunacaq addim sayi(susmaya gore 1 addimdir)
-- 3. Oxunan deyer NULL olarsa, evez olunancaq deyer
--LAG funksiyasi OVER bolmesinde ORDER BY bolmesinin olmasini teleb edir
select e.*,lag(salary) over( order by e.employee_id) from employees e;
select e.*,e.salary-lag(salary) over( order by e.employee_id) from employees e;

--Lead
--Bir sonraki setrde ki deyeri goturur
--Misal olaraq, deps ler uzre setrler uzre sonraki emp salary uzre ferqi tapmaq lazimdirsa LEAD istifade edeceyik
--LEAD func-i 3 hisseden ibaretdir 1. Deyeri oxunacaq SUTUN adi 2. Geri oxunacaq addim sayi(susmaya gore 1 addimdir)
-- 3. Oxunan deyer NULL olarsa, evez olunancaq deyer
--LEAD funksiyasi OVER bolmesinde ORDER BY bolmesinin olmasini teleb edir

select e.*,lead(salary) over( order by e.employee_id) from employees e;
select e.*,e.salary-lead(salary) over( order by e.employee_id) from employees e;

--FIRST_VALUE
--Kesim uzre ilk setrde olan deyeri qaytarir
--Kesim tetbiq edilerse her kesim uzre ilk deyeri hemin kesim uzre qaytarilan setrler yaninda gosterecekdir.
 
select e.*, first_value(e.salary) over() from employees e; 
select e.*, first_value(e.salary) over(partition by e.department_id) from employees e; 

--iller uzre satis saylari
select count(*),year_ from sales_data group by year_ order by year_ ;

--her il uzre satis ferqini mueyyen etmek
select a.cnt,a.year_,  a.cnt-lag(a.cnt) over(order by a.year_)  from (select count(*) as CNT,year_ from sales_data group by year_ order by year_ ) a;

--burada ilk setrde null olmasi sebebi deyerlerden birinin NULL olmasidir.
--eger en yuksek gelirli ili mueyyen etmek istesek o zaman 3 cu sutuna gore ORDER edib tapa bilerik.

select a.cnt,a.year_,  a.cnt-lag(a.cnt) over(order by a.year_)  from (select count(*) as CNT,year_ 
  from sales_data group by year_ order by year_ ) a order by 3 desc ;



se
