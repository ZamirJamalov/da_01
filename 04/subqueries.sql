select * 
    from sales_data;


    
--scalar type 
--2012 ci il en cox satilan   
--satis kanalindan yani  top 1(Top 2 ve daha cox istenilse column type-bax.) hemin kanal uzerinden
--2022 ci il neqeder mehsul satidigini oyrenmek lazimdir.
--ilk novbede top 1  satis kanali 2012 ci ile gore mueyyen etmek lazimdir.
--bunun ucun  sql yazaq
select sales_channel, count(*) 
  from sales_data 
    where year_=2012 
      group by sales_channel
        order by count(*) desc
           fetch next 1 rows only;
         
      
--bu sorgu neticede 2012 -ci il ucun top 1 satis kanalini eks etdirecek .
--daha sonra biz bu melumata esasen 2022 il uzre satis melumatlari gore bilerik.
--bunun ucun ya biz  sonuncu  sorgunun icrasi zamani elde edilen neticeni goturub sorgu 
--yaza bikerik ya da ki, bu yazdigimiz sorugunu scalar sorgu halina salib murekkeb sorgu yaza bilerik.
--ilk varianri tetbiq edek ve neticeni esas goturerek sql yazaq

select * 
  from sales_data 
     where sales_channel='Online'
       and year_=2022;



--diger variant olan  scalar sub query-ni yazaq . bunun ucun   yuxaridaki "Online" deyerini qaytaran sql-i  esas sql -e elave edeceyik.
--Amma oldugu kimi elave etsek xeta verecek. Beleki ,  esas  sql (-buna main query de deye bilerik),
--sert bolmesinde  yalniz 1 sayda deyer gozlediyi  halda ,icerideki sql (buna sub query deye bilerik),
-- hal-hazirda 2 sayda deyer(sales_channel, count uzre) qaytardigindan  sonlanmayacaq.
select * 
  from sales_data 
      where sales_channel=(
                select sales_channel, count(*) 
                      from sales_data 
                        where year_=2012 
                          group by sales_channel
                            order by count(*) desc
                               fetch next 1 rows only
      )
        and year_=2022;


--bunun ucun biz sub query de scalary liq sertini pozan ikinci deyeri silib ya da comment edib  umumi sorugunu 
--bu sekilde yazmaliyiq.

select * 
  from sales_data 
      where sales_channel=(
                select sales_channel  --, count(*) select
                      from sales_data 
                        where year_=2012 
                          group by sales_channel
                            order by count(*) desc
                               fetch next 1 rows only
      )
        and year_=2022;
        
--neticede  evvekli sorgudaki kimi 9 sayda setr  qaytardi. 
--amma evvelki sorgudan ferqli olaraq, manual elaveler aradan qaldirildi.
--daha bir elave odur ki, count deyerini sadece comment ederek  legv etmisik.
--yani sql hemin koment bolumunden sonra yazilanlari sadece comnent olaraq anladigindan 
--count(*)   -bu ifadeni sql -in islek terkib hissesi kimi gormur ve bu sebebden de sql  normal calisir.

--row subquery 
--2012  cu il uzre 4 rub- de satis kanallari uzre satis hecmi en yuksek olan rubu ve satis kanalini tapib
--bu bu melumatlar esasinda 2022 ci ilin hemin rubunde ve hemin satis kanali uzre satislara baxmaq lazimdir.

--ilk once subquery kimi istifade edeceyimiz sorgu hazirlamaga calisaq.

select sales_channel,Quarter,count(*) 
          from sales_data 
            where year_=2012 
              group by sales_channel,quarter
               order by count(*) desc
                   fetch next 1 rows only;
        
--daha sonra bunu main queryde bunu nezere alacagiq.
--burada da count bolmesini commente almaliyiq, eks halda xeta verecek.
--xetanin sebebi, main query 2 deyer gozlediyi halda sub query nin 3 deyer qaytarmasi olacaq.
select * 
   from sales_data 
     where (sales_channel,quarter)=(
            select sales_channel,Quarter--,count(*) 
          from sales_data 
            where year_=2012 
              group by sales_channel,quarter
                order by count(*) desc
                   fetch next 1 rows only
     ) 
      and year_=2022;


--column subquery 
--2012 -ci ilin 1-ci rubunde musterilere aid olkeler uzre satilan mehsullarin hecmine gore top 3 olkelerden hansilar uzre 
--hemin olkelere mehsullar 2023 ilde satilib

--yene de ilk novbede sub query yaratmaga calisaq.
--bunun ucun meselenin sertine baxiriq ve kohne tarixe gore bizden hansi neticeni gormek 
--isteyirlerse ona fokuslaniriq. Yani bizden sadece mehsullari isteyirler.
--bunun ucun biz product_id istifade edirik.

select customer_country,count(*)
    from sales_data 
       where year_=2012 
        and  quarter='Q1'
          group by customer_country
           order by count(*)
             fetch next 3 rows only;
             
--sub query 1-den cox  setr  qaytardigi ucun  
--main query de biz IN istifade etmeliyik eks halda = istifade etsek,
--bu beraberliyin sol ve saq terefinde deyer saylari ferqli oldugu ucun 
--xeta verecekdir. Bunu nezere alib IN  olan main query yazaq.
--burada da count bolmesini commente aliriq, xeta vermesin deye ,
--cunki main query  bir sutun uzre deyer gozlediyi halda sub query 2 sutun uzre deyer
--qaytardigindan xeta verecekdir.

select  * 
   from sales_data 
     where customer_country in (
             select customer_country--,count(*)
            from sales_data 
               where year_=2012 
                and  quarter='Q1'
                  group by customer_country
                   order by count(*)
                     fetch next 3 rows only
     )
      and  year_=2022;



  
          
          
--table  type sub query
select * from ( 
   select * from sales_data where year_=2012
) where  customer_country like '%a';

--bu sorgunun qaytardigi deyerler biznez baximindan mentiqli gorunmesede
--table tipli subqueryler mehz bu sekilde calisir.
--yani  sub  query  icra olunduqdabn sonra neticesini main query e qaytarir.
--main query artiq sanki cedvelden melumatlar nece cekerek calisirisa, hemin
--qaydaya uygun sub query nin neticeleri ile calisir. Hansi ki, sirgudan
--goruruk ki, sub query-nin neticelere gore  melumatlari filtr edir.



--correlated sub query
--  2022-ci ildə satılan məhsulun total qiymətinin butun
-- iller uzre satılan eyni məhsulun minimum total qiymətindən çox olduğu  melumatlari tapmaq
--

  
select product_name,total_price
  from sales_data  a
  where total_price>
  (
          select min(total_price)
           from  sales_data b
             where   a.product_name=b.product_name 
  )  
  and year_=2022
  order by total_price desc;
  

-- main query 
--Burada olan subquery evvelki subqueryler kimi onceden sitem terefinden icra olunmur.
--Subquery ni ayrica secib icra etmeye calissa xeta verecek, ona gore ki, hemin sorguda , mainquery -e aid sutun adina berkidilib.
--Mehz buna gore de burada ilk olaraq  main query calisir ve bu main query terefinden her defe yeni setr melumat oxunanda bu melumatlar
--subquery de olan "a.product_name=b.product_name" elaqelendirmeye gore 
--subqeury-ninde calismasini temin edir. Yani main query nin her oxunan setri ucun
--subquery sorgusu isledilir.
--baxmayaraq ki, yazdigimiz bu sorgu bize netice qaytarir, umumiyetle correlated subquerylerin
--istifadesi meslehet gorulmur , ona gore ki,  mainquery  sorgusunun
--qaytardigi setrlerin sayi qeder her defe subquery tekrar-tekrar calisir.
--bu da neticede sistemin performan gostericilerine menfi tesir gosterir. 
--Cixis yolu olaraq, alternativ usullardan istifadesi meslehet gorulur:
--relation quraraq bunu daha suretli isleyen sorguya cevire bilerik. relationlari novbeti derslerde daha etrali kececeyik
--yuxaridaki sorgunu relation tipli edek: 
  
 select a.product_name ,a.total_price
      from sales_data a  , (select product_name,min(total_price) as tt_price from sales_data group by product_name) b
      where a.product_name=b.product_name
      and year_=2022
      and a.total_price>b.tt_price
      order by  b.tt_price desc;


 
  --exists
  --subquery -de netice true olarsa ,mainquery de netice qayidir. eks halda ise netice qayitmir.
  --misal olaraq, 2010 -ci ilde Phone satis kanali uzerinden satis eden musterilerin 
  --siyahisi lazimdir. Bunun ucun exists -den istifade ede bilerik. 
 -- bu sql bize yalniz customer_id sutunu uzre melumatlari verecek.
 --eger sales_data uzere butun sutunlar uzre melumatlari isteyirikse
 --o zaman bu sql i column type subquery cevirmek lazimdir. 
 SELECT DISTINCT CUSTOMER_ID
FROM sales_data s1
WHERE EXISTS (
SELECT 1
FROM sales_data s2
WHERE s1.CUSTOMER_ID = s2.CUSTOMER_ID
AND s2.sales_channel = 'Phone'
and s2.year_=2010
);
    
--Eger tek customer_id deyil butun sutunlar uzre melumatlari gormek istesek 
--o zaman yuxaridaki  sorgunu column type subquery kimi istifaderek istediyimiz neticeni elde  bilerik.
select * from sales_data where customer_id in 
(
   SELECT DISTINCT CUSTOMER_ID
FROM sales_data s1
WHERE EXISTS (
SELECT 1
FROM sales_data s2
WHERE s1.CUSTOMER_ID = s2.CUSTOMER_ID
AND s2.sales_channel = 'Phone'
and s2.year_=2010
)
);
    
          
          
--CTE
--Common Table Expression Ve ya With clause istifade etmekle teyin etdiyimiz subquery-ler yazmaq olar.
--CTE esas mahiyyeti odur ki, boyuk sorgularda daha cox tekrarlanan mentiq hisseni bir defe yazmaqla,
--ona her defe istinad ederek, sorguda tekrarlan mentiq hisseni aradan qaldiraraq hem sorgunun hecmini azaldiriq hem de
--neticede sorgunun performansina musbet tesir gostermis olur.
--subquery category_sales adi vermekele bu adi asagida istifade etmis oluruq.

    WITH category_sales AS
    (
            SELECT
            PRODUCT_CATEGORY,
            SUM(TOTAL_PRICE) AS total_sales
            FROM
            sales_data
            WHERE
            YEAR_ = 2022
            GROUP BY
            PRODUCT_CATEGORY
    )
SELECT PRODUCT_CATEGORY,total_sales
FROM category_sales
ORDER BY total_sales DESC;

--diqet olunmali esas mesele odur ki, teyin edilmis category_sales esas sorguya yaxin yani onun terkibinde 
--istifade olunmalidir. Yani WITH le baslayan sorgu eger daha sonra ; vergulle sonlanirsa , bundan sonra 
--cagirilmasi mumkun deyildir. cunki o ilk ; den sonra quvvesini ititir.


    WITH category_sales AS
    (
            SELECT
            PRODUCT_CATEGORY,
            SUM(TOTAL_PRICE) AS total_sales
            FROM
            sales_data
            WHERE
            YEAR_ = 2022
            GROUP BY
            PRODUCT_CATEGORY
    )
SELECT PRODUCT_CATEGORY,total_sales
FROM category_sales
ORDER BY total_sales DESC;


--xeta verecekdir.
select * from categories_sales;




--Bir sorgu daxilinde bir nece subqueryleri WITH bolumunde tanida bilerik 
--misal olaraq, bizden her ilin her ayi ucun en cox ve en az satilan mehsullarin ID lerini ve saylarini
--elde etmek teleb olunur. bunun ucun bu sorgunu yaza bilerik. Burada coxluqlarin sayi 1-den cox oldugu ucun
--onlar arasinda  elaqelendirmeni qurmaq ucun JOIN lerden istifade etmek zeruretindeyik. JOIN -uzre 
--derslerimiz cedveller arasi elaqeleri kecende daha etrafli oyreneceyik.


WITH monthly_sales AS (
    SELECT EXTRACT(MONTH FROM to_Date(date_,'MM/DD/YYYY')) AS sale_month,
           EXTRACT(YEAR FROM to_Date(date_,'MM/DD/YYYY')) AS sale_year,
           product_id,
           SUM(quantity) AS total_quantity
    FROM sales_data
    GROUP BY EXTRACT(MONTH FROM to_Date(date_,'MM/DD/YYYY')), EXTRACT(YEAR FROM to_Date(date_,'MM/DD/YYYY')), product_id
),
max_sales AS (
    SELECT sale_month, sale_year, MAX(total_quantity) AS max_quantity
    FROM monthly_sales
    GROUP BY sale_month, sale_year
),
min_sales AS (
    SELECT sale_month, sale_year, MIN(total_quantity) AS min_quantity
    FROM monthly_sales
    GROUP BY sale_month, sale_year
)
SELECT ms.sale_year, ms.sale_month, ms.product_id, ms.total_quantity,
       CASE
           WHEN ms.total_quantity = mx.max_quantity THEN 'MAX'
           WHEN ms.total_quantity = mn.min_quantity THEN 'MIN'
           ELSE 'OTHER'
       END AS sales_category
FROM monthly_sales ms
JOIN max_sales mx ON ms.sale_month = mx.sale_month AND ms.sale_year = mx.sale_year
JOIN min_sales mn ON ms.sale_month = mn.sale_month AND ms.sale_year = mn.sale_year
WHERE ms.total_quantity = mx.max_quantity OR ms.total_quantity = mn.min_quantity
ORDER BY ms.sale_year, ms.sale_month, sales_category;





          
describe sales_data;








